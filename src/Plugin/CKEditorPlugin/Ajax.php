<?php

namespace Drupal\ckeditor_ajax\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginBase;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "ajax" plugin.
 *
 * @CKEditorPlugin(
 *   id = "ajax",
 *   label = @Translation("Ajax"),
 * )
 */
class Ajax extends CKEditorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return 'libraries/ajax/plugin.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return ['xml'];
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [];
  }

}
